import tkinter
import mysql.connector
from mysql.connector import Error

def create_connection():
    connection = None
    try:
        connection = mysql.connector.connect(
            host="localhost",
            user="root",
            password="root",
            database="wise1"
        )
        print("Connection to MySQL DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection

def save_text_to_database(text, valid_file_name):
    connection = create_connection()
    cursor = connection.cursor()
    query = "INSERT INTO textFiles (text, valid_file_name) VALUES (%s, %s)"
    cursor.execute(query, (text, valid_file_name))
    connection.commit()
    print("Text and valid file name saved to MySQL DB")
    cursor.close()
    connection.close()

def register_user(self, username_entry, password_entry):
    username = username_entry.get()
    password = password_entry.get()

    connection = create_connection()
    cursor = connection.cursor()
    query = "INSERT INTO users (username, password) VALUES (%s, %s)"
    cursor.execute(query, (username, password))
    connection.commit()
    print("User registered successfully")
    cursor.close()
    connection.close()

    username_entry.delete(0, tkinter.END)
    password_entry.delete(0, tkinter.END)

    self.frame3.pack_forget()
    self.frame1.pack(fill=tkinter.BOTH, expand=True)

def login_user(self, login_username_entry, login_password_entry):
    username = login_username_entry.get()
    password = login_password_entry.get()

    connection = create_connection()
    cursor = connection.cursor()
    query = "SELECT * FROM users WHERE username=%s AND password=%s"
    cursor.execute(query, (username, password))
    result = cursor.fetchone()
    cursor.close()
    connection.close()

    if result:
        self.logged_in = True
        self.frame4.pack_forget()
        self.frame1.pack(fill=tkinter.Tk.BOTH, expand=True)
    else:
        print("Invalid username or password")